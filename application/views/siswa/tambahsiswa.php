<div class="row">
	<div class="col-md-12">
		<div class="box box-primary">
                <div class="box-header">
                  <h3 class="box-title">Quick Example</h3>
                </div><!-- /.box-header -->
                <!-- form start -->
                <form role="form">
                  <div class="box-body">
                    <div class="form-group">
                      <label for="inputnis">N.I.S</label>
                      <input type="number" class="form-control" id="inputnis" placeholder="Masukkan Nomor Induk Siswa">
                    </div>
                    <div class="form-group">
                      <label for="inputnama">Nama</label>
                      <input type="text" class="form-control" id="inputnama" placeholder="Masukkan Nama Siswa">
                    </div>
                   
                    <div class="checkbox">
                      <label>
                        <input type="checkbox"> Saya Setuju
                      </label>
                    </div>
                  </div><!-- /.box-body -->

                  <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Simpan</button>
                  </div>
                </form>
              </div><!-- /.box -->
	</div>
</div>